# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=/Users/allen/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="robbyrussell"

# Set list of themes to load
# Setting this variable when ZSH_THEME=random
# cause zsh load theme from this variable instead of
# looking in ~/.oh-my-zsh/themes/
# An empty array have no effect
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git
  yarn-autocompletions
  autojump
)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
alias mountw='sudo mount -t cifs //192.168.1.10/public /home/keke/smb'
alias mountu='sudo mount -t exfat /dev/sdb1 /media/u'
alias rsyncwiki='rsync -avzP --exclude=diary ~/Dropbox/Vimwiki gnometwe@imkeke.net:www/imkeke/'
alias rsyncbin='rsync -avzP ~/Dropbox/Scripts/ vps:bin/'
alias rsyncblog='nvm use 0.9.1 && cd ~/Dropbox/blog/ && wintersmith build && rsync -avz ~/Dropbox/blog/build/ keke:~/public_html/imkeke.net/'
alias rsyncbloga="rsync -avz -e 'ssh -p 26875' ~/blog/build/ keke@104.224.140.18:~/www/blog_keke_im/"
alias startxp='VBoxManage startvm xp'
alias clcf='/System/Library/Frameworks/CoreServices.framework/Frameworks/LaunchServices.framework/Support/lsregister -kill -r -domain local -domain system -domain user'
alias pythonserver='python -m SimpleHTTPServer'
alias gfwon='networksetup -setautoproxyurl "网线" http://p.getqujing.com/paths/ldhng11q.pac'
alias gfwoff='networksetup -setautoproxystate "网线" off'
alias gfwonwifi='networksetup -setautoproxyurl "Wi-Fi" http://p.getqujing.com/paths/ldhng11q.pac'
alias gfwoffwifi='networksetup -setautoproxystate "Wi-Fi" off'
alias http_proxy_set='export http_proxy="http://thenorth.f.getqujing.net:34937"'
alias sambarserver='sudo launchctl load -w /Library/LaunchAgents/org.samba.nmbd.plist && sudo launchctl load -w /Library/LaunchAgents/org.samba.smbd.plist'
alias showhide='defaults write ~/Library/Preferences/com.apple.finder AppleShowAllFiles -bool true'
alias cpjd="rsync -aP"
alias autolinkworkcomputer="autossh -NfR 8758:localhost:22 vps"
alias lr='ls -R | grep ":$" | sed -e '\''s/:$//'\'' -e '\''s/[^-][^\/]*\//--/g'\'' -e '\''s/^/   /'\'' -e '\''s/-/|/'\'''
alias makegif='ffmpeg -i in.mov -s 600x400 -pix_fmt rgb24 -r 10 -f gif - | gifsicle --optimize=3 --delay=3 > out.gif'
alias vimn='vim -u NONE -N'
alias vimt='vim -u ~/.vimterminal/vimrc -N'
alias getchmodv='stat -f "%OLp"'
alias o="cd ~/Sites/cit/one-drupal/docroot/themes/custom/one && grunt watch"
alias n="cd /Users/allen/Sites/cit/nestle-cn-hr-portal/docroot/themes/custom/nestle && npm start"
alias nImport="MYSQL_PWD=3.1415926 mysqldump --column-statistics=0 -unshr -h192.168.3.87 nshr > db_backup.sql && MYSQL_PWD=0000 mysql -uroot -h127.0.0.1 nestle < db_backup.sql && rm db_backup.sql && rsync -avzP citadmin@192.168.3.175:/var/www/nshr/docroot/sites/default/files/ ~/Sites/cit/nestle-cn-hr-portal/docroot/sites/default/files/"
alias bImport="MYSQL_PWD=3.1415926 mysqldump --column-statistics=0 -ubpiqj -h192.168.3.87 bpiqj > db_backup.sql && MYSQL_PWD=0000 mysql -uroot -h127.0.0.1 bpiqj < db_backup.sql && rm db_backup.sql && rsync -avzP citadmin@192.168.3.175:/var/www/bpiqj/docroot/sites/default/files/ ~/Sites/cit/dcx-drupal/docroot/sites/default/files/"
alias cImport="MYSQL_PWD=3.1415926 mysqldump --column-statistics=0 -uroot -h192.168.7.249 cityu > db_backup.sql && MYSQL_PWD=0000 mysql -uroot -h127.0.0.1 cityu < db_backup.sql && rm db_backup.sql && rsync -avzP citadmin@192.168.3.175:/var/www/cityu/docroot/sites/default/files/ ~/Sites/cit/cityu/docroot/sites/default/files/"
alias caImport="MYSQL_PWD=3.1415926 mysqldump --column-statistics=0 -uroot -h192.168.7.249 cityu > db_backup.sql && MYSQL_PWD=0000 mysql -uroot -h127.0.0.1 cityua < db_backup.sql && rm db_backup.sql && rsync -avzP citadmin@192.168.3.175:/var/www/cityu/docroot/sites/default/files/ ~/Sites/cit/cityu_a/docroot/sites/default/files/"

function blt() {
  if [[ ! -z ${AH_SITE_ENVIRONMENT} ]]; then
    PROJECT_ROOT="/var/www/html/${AH_SITE_GROUP}.${AH_SITE_ENVIRONMENT}"
  elif [ "`git rev-parse --show-cdup 2> /dev/null`" != "" ]; then
    PROJECT_ROOT=$(git rev-parse --show-cdup)
  else
    PROJECT_ROOT="."
  fi

  if [ -f "$PROJECT_ROOT/vendor/bin/blt" ]; then
    $PROJECT_ROOT/vendor/bin/blt "$@"

  # Check for local BLT.
  elif [ -f "./vendor/bin/blt" ]; then
    ./vendor/bin/blt "$@"

  else
    echo "You must run this command from within a BLT-generated project."
    return 1
  fi
}


export PATH=$PATH:/Applications/MySQLWorkbench.app/Contents/MacOS

export ANDROID_HOME=$HOME/Library/Android/sdk
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools
export PATH=$PATH:$ANDROID_HOME/emulator
export REACT_EDITOR=mvim

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
